# test_specs.py
"""Python Essentials: Testing.
Lehner White
"""

import specs
import pytest
import numpy as np

# Problem 1: Test the addition and smallest factor functions from specs.py
def test_addition():
    assert specs.addition(2,4) == 6
    return

def test_smallest_factor():
    assert specs.smallest_factor(1) == 1
    assert specs.smallest_factor(3) == 3
    assert specs.smallest_factor(6) == 2
    return

# Problem 2: Test the operator function from specs.py
def test_operator():
    with pytest.raises(Exception) as e:
        specs.operator(3, 3, 20)
    assert e.typename == 'ValueError'
    assert e.value.args[0] == "Oper should be a string"

    with pytest.raises(Exception) as e:
      specs.operator(3, 3, '*/')
    assert e.typename == 'ValueError'
    assert e.value.args[0] == "Oper should be one character"
    
    with pytest.raises(Exception) as e:
      specs.operator(3, 0, '/')
    assert e.typename == 'ValueError'
    assert e.value.args[0] == "You can't divide by zero!"

    assert specs.operator(3, 1, '/') == 3/1.
    assert specs.operator(3, 1, '-') == 2
    assert specs.operator(3, 1, '+') == 4
    assert specs.operator(3, 1, '*') == 3

    with pytest.raises(Exception) as e:
      specs.operator(3, 0, 'a')
    assert e.typename == 'ValueError'
    assert e.value.args[0] == "Oper can only be: '+', '/', '-', or '*'"
    
    return

# Problem 3: Finish testing the complex number class
@pytest.fixture
def set_up_complex_nums():
    number_1 = specs.ComplexNumber(1, 2)
    number_2 = specs.ComplexNumber(5, 5)
    number_3 = specs.ComplexNumber(2, 9)
    return number_1, number_2, number_3

def test_complex_norm(set_up_complex_nums):
  number_1, number_2, number_3 = set_up_complex_nums
  assert number_2.norm() == np.sqrt(50)

def test_complex_addition(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert number_1 + number_2 == specs.ComplexNumber(6, 7)
    assert number_1 + number_3 == specs.ComplexNumber(3, 11)
    assert number_2 + number_3 == specs.ComplexNumber(7, 14)
    assert number_3 + number_3 == specs.ComplexNumber(4, 18)

def test_complex_subtraction(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert number_1 - number_2 == specs.ComplexNumber(-4, -3)
    assert number_1 - number_3 == specs.ComplexNumber(-1, -7)
    assert number_2 - number_3 == specs.ComplexNumber(3, -4)
    assert number_3 - number_3 == specs.ComplexNumber(0, 0)

def test_complex_multiplication(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert number_1 * number_2 == specs.ComplexNumber(-5, 15)
    assert number_1 * number_3 == specs.ComplexNumber(-16, 13)
    assert number_2 * number_3 == specs.ComplexNumber(-35, 55)
    assert number_3 * number_3 == specs.ComplexNumber(-77, 36)

def test_complex_division(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert number_1 / number_1 == specs.ComplexNumber(1, 0)
    
    with pytest.raises(Exception) as e:
      number_3 / specs.ComplexNumber(0, 0)
    assert e.typename == 'ValueError'
    assert e.value.args[0] == "Cannot divide by zero"

def test_complex_print(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert str(number_2) == "5+5i"

# Problem 4: Write test cases for the Set game.

def saver(array, filename=None):
  np.savetxt('test_set.txt', array, delimiter='', fmt='%i')
  if filename:
    np.savetxt(filename, array, delimiter='', fmt='%i')

def test_set_input():
  bad_size = np.array([[2, 2, 2, 2]]*11)
  saver(bad_size, './hands/bad_size.txt')

  with pytest.raises(Exception) as e:
    specs.play_set('test_set.txt')
  assert e.typename == 'ValueError'
  assert e.value.args[0] == "I need 12 cards"

  bad_entry = np.array([[2, 2, 2, 3]]*12)
  saver(bad_entry, './hands/bad_entry.txt')

  with pytest.raises(Exception) as e:
    specs.play_set('test_set.txt')
  assert e.typename == 'ValueError'
  assert e.value.args[0] == "Attributes need to be: 0, 1, or 2"

  bad_len = np.array([[2, 2, 2]]*12)
  saver(bad_len, './hands/bad_len.txt')

  with pytest.raises(Exception) as e:
    specs.play_set('test_set.txt')
  assert e.typename == 'ValueError'
  assert e.value.args[0] == "Cards need exactly 4 attributes"

  sol = specs.play_set('./hands/new_set.txt')
  real_sol = len([np.array([[ 1.,  1.,  2.,  1.],
       [ 0.,  0.,  1.,  2.],
       [ 2.,  2.,  0.,  0.]]), np.array([[ 0.,  0.,  0.,  0.],
       [ 0.,  0.,  0.,  1.],
       [ 0.,  0.,  0.,  2.]]), np.array([[ 0.,  0.,  0.,  0.],
       [ 1.,  1.,  1.,  1.],
       [ 2.,  2.,  2.,  2.]]), np.array([[ 0.,  0.,  0.,  1.],
       [ 1.,  1.,  1.,  0.],
       [ 2.,  2.,  2.,  2.]]), np.array([[ 0.,  0.,  0.,  2.],
       [ 1.,  1.,  1.,  2.],
       [ 2.,  2.,  2.,  2.]]), np.array([[ 1.,  1.,  1.,  1.],
       [ 1.,  1.,  1.,  2.],
       [ 1.,  1.,  1.,  0.]])])

  good_entries = [sol == real_sol]
  assert all(good_entries) == True

