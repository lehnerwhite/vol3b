"""
Lehner White
March 13, 3017
Lab 16
"""
from __future__ import division
from pymongo import MongoClient
from pprint import pprint
import pymongo
import json
import re

### Problems ###
mc = MongoClient()
mydb = mc.db1
rest = mydb.collection1

def prob1():
    #clear out rest in case we have already loading something into it
    rest.drop()

    # go through the file 'restaurants.json' and load all of the restaurants
    # into the rest collection
    with open('restaurants.json', 'r') as f:
        for res in f:
            rest.insert(json.loads(res))
    return 

def prob2():
    with open('mylans_bistro.json', 'r') as f:
        for res in f:
            rest.insert(json.loads(res))
    for res in rest.find({'closing_time':"1800"}):
        print(res['name'])
    return 
     
def prob3():
    man = rest.find({'borough':'Manhattan'}).count()
    grade_A = rest.find({'grades.grade':'A'}).count()
    north_sorted = rest.find().sort([('address.coord.1',-1)])[:10]
    grill = re.compile('grill',re.IGNORECASE)
    name_grill = rest.find({'name':{'$regex':grill}})
    
    # print out results
    res3 = []
    for res in north_sorted:
        res3.append(res['name'])
    
    res4 = []
    for res in name_grill:
        res4.append(res['name'])

    return man, grade_A, res3, res4 

def prob4():
    grill = re.compile('grill',re.IGNORECASE)
    name_grill = rest.find({'name':{'$regex':grill}})
    try:
        while True:
            res = name_grill.next()
            replacement = re.sub(grill, 'Magical Fire Table', res['name'])
            rest.update({'restaurant_id': res['restaurant_id']}, {"$set":{'name': replacement}})
    except:
        pass

    for res in rest.find():
        rest.update(res, {'$set': {'restaurant_id':str(int(res['restaurant_id'])+1000)}})

    rest.remove({'grades.grade':'C'})
    return

