# nameserver.py
"""
Vol 3B: Web Tech 1 (Internet Protocols). Auxiliary file.
Lehner White
"""

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import urlparse
import os

names = {   "Babbage": "Charles",
            "Berners-Lee": "Tim",
            "Boole": "George",
            "Cerf":"Vint",
            "Dijkstra":"Edsger",
            "Hopper":"Grace",
            "Knuth":"Donald",
            "von Neumann":"John",
            "Russel":"Betrand",
            "Shannon":"Claude",
            "Turing":"Alan"        }

class NameServerHTTPRequestHandler(BaseHTTPRequestHandler):
    """Custom HTTPRequestHandler class"""

    def do_GET(self):
        """Handle GET command"""
        self.send_response(200)

        parsed_path = urlparse.urlparse(self.path)
        try:
            params = dict([p.split('=') for p in parsed_path[4].split('&')])
        except:
            params = {}

        # Send header first
        self.send_header("Content-type","text-html")
        self.end_headers()

        # Send content to client
        try:
            in_string = params["lastname"]

            if in_string in names.keys():
                self.wfile.write(names[params["lastname"]])
            elif in_string == "AllNames":
                output = ""
                for last_name in names.keys():
                    first_name = names[last_name]
                    output += "{}, {}\n".format(last_name, first_name)
                self.wfile.write(output)
            else:
                valid_names = [last_name for last_name in names.keys() if last_name.startswith(in_string)]
                output = ""
                for last_name in valid_names:
                    first_name = names[last_name]
                    output += "{}, {}\n".format(last_name, first_name)
                self.wfile.write(output)
        except:
            self.wfile.write("I don\'t know that person.")
        
        return

    def do_PUT(self):
        """Handle PUT command"""
        # Get the input
        length = int(self.headers['Content-Length'])
        content = self.rfile.read(length)
        
        # Same as the finction for do_GET
        self.send_response(200)

        parsed_path = urlparse.urlparse(self.path)
        try:
            params = dict([p.split('=') for p in parsed_path[4].split('&')])
        except:
            params = {}

        self.send_header("Content-type","text-html")
        self.end_headers()

        # Adding the new name to the dictionary
        last_name = params["lastname"]
        first_name = params["firstname"]
        names[last_name] = first_name
        return 


def run():
    # Print("http server is starting...")
    server_address = ("0.0.0.0", 8000)
    httpd = HTTPServer(server_address, NameServerHTTPRequestHandler)
    print("http server is running...")
    httpd.serve_forever()

if __name__ == '__main__':
    run()
