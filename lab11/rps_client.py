import socket
from random import randint

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

ip = '0.0.0.0'
port = 33498
client.connect((ip, port))

size = 2048 # Block size of 20 bytes
keep_playing = True

while keep_playing:
    move = raw_input('Enter move (rock, paper or scissors): ')

    client.send(move)
    result = client.recv(size)
    print result
    if result == 'you win':
        break

client.close()
