import socket
from random import randint
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

address = '0.0.0.0' # Default address that specifies the local machine and allows connection on all interfaces
s.bind((address, 33498)) # Bind the socket to a port 33498, which was arbitrarily chosen
s.listen(1) # Tell the socket to listen for incoming connection

size = 2048 # Block size of 20 bytes
conn, addr = s.accept() # conn is our new socket object for receiving/sending data
print "Accepting connection from:", addr

while True:
    move = conn.recv(size) # Read 20 bytes from the incoming connection
    if not move: # Terminate the connection if data stops arriving (no more blocks to receive)
        break
    if move != 'rock' and  move != 'paper'  and move != 'scissors':
        conn.send('invalid move')
    else:
        win = 'you win'
        loss = 'you lose'
        draw = 'draw'
        
        counter = randint(0,2)
        # 0 = rock
        # 1 = paper
        # 3 = scissors

        print("")
        if move == 'rock':
            print('client played rock')
            if counter == 0:
                print('host played rock')
                conn.send(draw)
            elif counter == 1: 
                print('host played paper')
                conn.send(loss)
            elif counter == 2:
                print('host played scissors')
                conn.send(win)
        elif move == 'paper':
            print('client played paper')
            if counter == 0:
                print('host played rock')
                conn.send(win)
            elif counter == 1: 
                print('host played paper')
                conn.send(draw)
            elif counter == 2:
                print('host played scissors')
                conn.send(loss)
        elif move == 'scissors':
            print('client played scissors')
            if counter == 0:
                print('host played rock')
                conn.send(loss)
            elif counter == 1:
                print('host played paper')
                conn.send(win)
            elif counter == 2:
                print('host played scissors')
                conn.send(draw)
        print("")

conn.close()
