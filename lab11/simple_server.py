import socket
import time

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

address = '0.0.0.0' # Default address that specifies the local machine and allows connection on all interfaces
s.bind((address, 33498)) # Bind the socket to a port 33498, which was arbitrarily chosen
s.listen(1) # Tell the socket to listen for incoming connection

size = 2048 # Block size of 20 bytes
conn, addr = s.accept() # conn is our new socket object for receiving/sending data
print "Accepting connection from:", addr
while True:
    data = conn.recv(size) # Read 20 bytes from the incoming connection
    if not data: # Terminate the connection if data stops arriving (no more blocks to receive)
        break
    data += str(" " + time.strftime('%H:%M:%S'))
    conn.send(data) # Send the data back to the client
conn.close()
