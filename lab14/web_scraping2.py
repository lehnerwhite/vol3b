"""Volume III: Web Scraping 2.
Lehner White
Jan 31, 2017
"""
from bs4 import BeautifulSoup as bs
import pandas as pd
import urllib2
import re
from matplotlib import pyplot as plt
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


# Problem 1
def Prob1():
    """Load the Big Bank Info file and return a pandas DataFrame
    containing Bank Name, Rank, ID, Domestic Assets, and Domestic Branches
    for JP Morgan, Capital One, and Discover banks.
    """
    soup = bs(open("federalreserve.htm"), 'html.parser')
    banks = soup.find_all(text=re.compile('JPMORGAN|CAPITAL ONE|DISCOVER'))
    df = pd.DataFrame(columns=['Bank Name','Rank','ID','Domestic Assets','Domestic Branches'])
    
    for i, bank in enumerate(banks):
        info = []
        items = [1,3,5,13,19]
        for idx, x in enumerate(bank.parent.parent):
            if idx in items:
                info.append(x.string)
        df.loc[i] = info
    return df

# Problem 2
def Prob2():
    """Use urllib2 and BeautifulSoup to return the actual max temperature,
    the tag containing the link for 'Next Day', and the url associated
    with that link.
    """
    url = "https://math.byu.edu/weather/www.wunderground.com/history/airport/KSLC/2015/1/1/DailyHistory.html?req_city=&req_state=&req_statename=&reqdb.zip=&reqdb.magic=&reqdb.wmo="

    content = urllib2.urlopen(url).read()
    soup = bs(content, 'html.parser')
    
    actual_max = soup.find(text=re.compile('Max Temperature')).parent.parent.parent.contents[3].text.split()[0]
    next_day_tag = soup.find(class_='next-link')
    next_day_link = next_day_tag.contents[0]['href']
    return actual_max, next_day_tag, next_day_link

# Problem 3
def Prob3():
    """Mimic the Wunderground Weather example to create a list of average
    max temperatures of the year 2015 in San Diego. Use matplotlib to draw
    a graph depicting the data, then return the list.
    """
    url = "https://math.byu.edu/weather/www.wunderground.com/history/airport/KSLC/2015/1/1/DailyHistory.html?req_city=&req_state=&req_statename=&reqdb.zip=&reqdb.magic=&reqdb.wmo="
    base = "https://math.byu.edu/weather/www.wunderground.com/history/airport/KSLC/2015/"
    content = urllib2.urlopen(url).read()
    soup = bs(content, 'html.parser')
    
    max_temps = []
    month = 1

    while(month < 3):
        print month
        actual_max = soup.find(text=re.compile('Max Temperature')).parent.parent.parent.contents[5].text.split()[0]
        print actual_max 
        max_temps.append(int(actual_max))
        
        next_day_tag = soup.find(class_='next-link')
        next_day_link = next_day_tag.contents[0]['href']
        print next_day_link
        
        if '../..' in next_day_link:
            next_day_link = next_day_link[8:]
            month += 1
        else:
            next_day_link = next_day_link[3:]
        
        url = "{}{}/{}".format(base, month, next_day_link)
        
        content = urllib2.urlopen(url).read()
        soup = bs(content, 'html.parser')

    plt.plot(max_temps)
    plt.xlabel('Day of Year')
    plt.ylabel('Maximum Temperature')
    plt.show()
    return max_temps

# Problem 4
def Prob4():
    """Load the selected option into BeautifulSoup. Find the requested
    information for the selected option and make a SQL table that
    stores this information.
    """

    url = "http://www.google.com/finance"
    base = "http://www.google.com/"

    content = urllib2.urlopen(url).read()
    soup = bs(content, 'html.parser')

    df = pd.DataFrame(columns=['Name','Abbreviation','Percent Change','Mkt Cap'])

    tables = soup.find_all('table')[9]
    
    sectors = ['Energy','Basic Materials','Industrials','Cyclical Cons. Goods', 'Financials','Healthcare','Technology','Telecommunications','Utilities']

    for idx, sector in enumerate(sectors):
        link = tables.find(text=re.compile(sector)).parent['href']
        sector_url = "{}{}".format(base, link[1:])

        temp_content = urllib2.urlopen(sector_url) 
        temp_soup = bs(temp_content, 'html.parser')

         
        top = temp_soup.find('table', {'class':'topmovers'})
        mkt_cap = top.td.find_all(text=True)[18][:-1]
        name = top.find_all('a')[0].text
        abbrev = top.find_all('a')[1].text
        pct_change = top.find('span').text
        data = [name, abbrev, pct_change, mkt_cap]
        df.loc[idx] = data

    return df

# Problem 5
def Prob5():
    """Use selenium to return a list of all the a tags containing each of the
    30 NBA teams. Return only one tag per team.
    """
    driver = webdriver.Chrome('./chromedriver')
    
    url = 'http://stats.nba.com/league/team/#!/?sort=W&dir=1'
    driver.get(url)
    soup = bs(driver.page_source, 'lxml')
    
    teams = soup.find('div', split='splits.TeamID').find_all('option')
    
    return teams[1:]

print Prob5()
