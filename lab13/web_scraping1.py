"""Volume 3B: Web Scraping 1. Spec file."""
from bs4 import BeautifulSoup
import re

html_doc = """
    <html><head><title>The Three Stooges</title></head>
    <body>
    <p class="title"><b>The Three Stooges</b></p>
    <p class="story">Have you ever met the three stooges? Their names are
    <a href="http://example.com/larry" class="stooge" id="link1">Larry</a>,
    <a href="http://example.com/mo" class="stooge" id="link2">Mo</a> and
    <a href="http://example.com/curly" class="stooge" id="link3">Curly</a>;
    and they are really hilarious.</p>
    <p class="story">...</p>
    """

# Problem 1
def prob1(filename):
    """
    Find all of the tags used in a particular .html file and the value of
    the 'type' attribute.

    Inputs:
        filename: Name of .html file to parse

    Outputs:
        - A set of all of the tags used in the .html file
        - The value of the 'type' attributes for the style tag
    """
    f = open(filename, 'r')
    doc = f.read()

    soup = BeautifulSoup(doc, 'html.parser')
    tags_used = [tag.name for tag in soup.find_all()]
    value = soup.find('style').attrs['type']
    return tags_used, value

# Problem 2
def prob2():
    """Prints (not returns) the prettified
    string for the Three Stooges HTML.
    """
    soup = BeautifulSoup(html_doc, 'html.parser')
    print(soup.prettify())

# Problem 3
def prob3():
    """Returns [u'title'] from the Three Stooges soup"""
    soup = BeautifulSoup(html_doc, 'html.parser')
    return soup.title.string

# Problem 4
def prob4():
    """Returns u'Mo' from the Three Stooges soup"""
    soup = BeautifulSoup(html_doc, 'html.parser')
    for child in soup.descendants:
        if child == 'Mo':
            value = child.string
    return value

# Problem 5
def prob5(method):
    """Returns the u'More information...' using two different methods.
    If method is 1, it uses first method. If method is 2, it uses
    the second method.
    """
    soup = BeautifulSoup(open('example1.htm'), 'html.parser')
    if method == 1:
        for child in soup.descendants:
            if child == 'More information...':
                value = child.string
        return value
    if method == 2:
        return soup.div.contents[5].string

# Problem 6
def prob6(method):
    """Returns the tag associated with the "More information..."
    link using two different methods. If method is 1, it uses the
    first method. If method is 2, it uses the second method.
    """
    soup = BeautifulSoup(open('example1.htm'), 'html.parser')
    if method == 1:
        return soup.find(href="http://www.iana.org/domains/example")
    if method == 2:
        return soup.find('a')

# Problem 7
def prob7():
    """Loads 'SanDiegoWeather.htm' into BeautifulSoup and prints
    (not returns) the tags referred to the in the Problem 7 questions.
    """
    soup = BeautifulSoup(open('SanDiego.htm'), 'html.parser')
    # Question 1
    print soup.find(class_="history-date") 
    # Question 2
    print soup.find(class_="previous-link").contents[0]
    print soup.find(class_="next-link").contents[0]
    # Question 3
    print soup.find(text=re.compile('Max Temperature')).parent.parent.parent.find_all('span')[1].find('span')

# Problem 8
def prob8():
    """Loads 'Big Data dates.htm' into BeautifulSoup and uses find_all()
    and re to return a list of all tags containing links to bank data
    from September 30, 2003 to December 31, 2014.
    """
    soup = BeautifulSoup(open("Big-Data-dates.htm"), 'html.parser')
    return soup.find_all(text=re.compile('200[3-9]|201[0-4]'), href=True)

