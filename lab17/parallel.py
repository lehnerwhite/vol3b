# parallel1.py
from __future__ import division
import numpy as np
import itertools
from time import time
from matplotlib import pyplot as plt
from ipyparallel import Client

client = Client()
dview = client[:]

def prob3(n=1000000):
    """
    Write a function that returns the mean, max, and min for n draws
    from the standard normal distribution where n is default to 1,000,000.
    Use apply_sync() to execute this function across all available engines.
    Print the results as returned by each engine.
    """
    def draw():
        import numpy as np
        a = np.random.randn(n)
        return a.mean(), a.max(), a.min()

    
    results  = dview.apply_sync(draw)
    results = np.array(list(results))
    
    means = results[:,0]
    maxs = results[:,1]
    mins = results[:,2]
    
    return means, maxs, mins

def prob4():
    """
    Time the function from the previous problem in parallel and serially. Run
    apply_sync() on the function to time in parallel. To time the function
    serially, run the function in a for loop n times, where n is the number
    of engines on your machine. Print the results.
    """
    for n in [1000000, 5000000,10000000, 15000000]:
        print("\nFor n = {}".format(n))
        def draw():
            import numpy as np
            a = np.random.randn(n)
            return a.mean(), a.max(), a.min()
       
        start = time()
        results  = dview.apply_sync(draw)
        end = time()
        
        parallel = end-start
        print("Time Elapsed for parallel computation: {}".format(parallel))
        
        start = time()
        for i in xrange(4):
            draw()
        end = time()
            
        serial = end - start
        print("Time Elapsed for serial computation: {}".format(serial))
        print("Parallel was {} times faster!".format(serial/parallel))

def prob5(N):
    """
    Accept an integer N that represents the number of draws to take from the
    normal distribution for the distribution X. Take 500,000 draws from this
    distribution and plot a histogram of the results. Split the load evenly
    among all available engines and make the function flexible to the number
    of engines running.
    """
    def draw(n, procs): 
        import numpy as np
        draws = []
        for i in xrange(500000//procs):
            a = np.random.randn(n)
            draws.append(a.max())

        return draws

    procs = len(client.ids)
    draws_list = dview.apply_sync(draw, N, procs)
    draws = [point for draws in draws_list for point in draws]
    plt.hist(draws, bins=20)
    plt.title('Distribution of Samples from Max Draws')
    plt.ylabel('Number of Samples')
    plt.xlabel('Value of Max Draw')
    plt.show()

def parallel_trapezoidal_rule(f, a, b, n=200):
    """
    Write a function that accepts a function handle, f, bounds of integration,
    a and b, and a number of points to use, n. Split the interval of
    integration evenly among all available processors and use the trapezoidal
    rule to numerically evaluate the integral over the interval [a,b].

    Parameters:
        f (function handle): the function to evaluate
        a (float): the lower bound of integration
        b (float): the upper bound of integration
        n (int): the number of points to use, defaults to 200
    Returns:
        value (float): the approximate integral calculated by the
            trapezoidal rule
    """
    h = (b - a) / (n-1)
    X = np.linspace(a, b, n)
    dview.execute("import numpy as np") 
    dview['h'] = h
    dview.scatter("low_point", f(X[:-1]))
    dview.scatter("up_point", f(X[1:]))

    dview.execute("sol = (h/2.) * np.sum(low_point + up_point)")
   
    solution = dview.gather("sol",block=True)
    return sum(solution)

