from mpi4py import MPI
from mpi4py.MPI import ANY_SOURCE
from sys import argv
import numpy as np

"""
Write a script that runs on n processes and passes an n by 1 array
of random values from the ith process to the i+1th process (or to the 0th
process for the if i is the last process).  Have each process identify
itself and what it is doing:

    e.g. "Process {process_num} sending array {array to process_num + 1}"
    and "Process {process_num} just received {array from process_num - 1}"
"""

COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()
num_proc = COMM.Get_size()

a = np.random.rand(num_proc)
b = np.zeros(num_proc)

print 'Process {} sending array {}'.format(RANK, a, (RANK+1)%num_proc)
COMM.Send(a, dest = (RANK+1) % num_proc)

COMM.Recv(b, source = ANY_SOURCE)
print 'Process {} just received {}'.format(RANK, b)

