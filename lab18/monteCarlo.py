from __future__ import division
from mpi4py import MPI
from sys import argv
import numpy as np

"""
Write a script that runs a Monte Carlo simulation to approximate the volume
of an m-dimensional unit sphere.  Your script should accept the following
parameters as command line arguments:
    n - number of processes (passed as normal into mpirun)
    m - dimension of unit sphere
    p - number of points to use in simulation

For your final answer, print out the approximated volume from your simulation.
"""

m, sample_points = int(argv[1]), int(argv[2])

COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()

# local calculations
points = np.random.rand(sample_points, m)
vals = np.linalg.norm(points, axis=1)
tot_in = sum(vals < 1)
loc_vol = 2**m * tot_in / sample_points

#global calculations
tot_vol = COMM.gather(loc_vol, root=0)

if RANK == 0:
    tot_vol = np.mean(np.array(tot_vol))
    print 'Volume of unit {}-sphere is {}'.format(m,tot_vol)

