from mpi4py import MPI
from sys import argv
import numpy as np


"""
Write a script that runs on two processes and passes an n by 1 array
of random values from one process to the other.  Have each process identify
itself and what it is doing:

    e.g. "Process 1 sending array {array}"
    and "Process 0 just received {array}"
"""

# Get n passed in by user
n = int(argv[1])

COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()

if RANK == 1:
    send_v = np.random.rand(n)
    print('Process 1 sending array {}'.format(send_v))
    COMM.Send(send_v, dest=0)

if RANK == 0:
    rec_v = np.zeros(n)
    COMM.Recv(rec_v, source=1)
    print('Process 0 just received {}'.format(rec_v))
