from bokeh.plotting import figure, show
from bokeh.models import WMTSTileSource, ColumnDataSource
from pyproj import Proj, transform
import requests
import numpy as np
from bokeh.io import curdoc
from bokeh.layouts import column, row, layout, widgetbox

from_proj = Proj(init="epsg:4326")
to_proj = Proj(init="epsg:3857")

def convert(longitudes, latitudes):
    x_vals = []
    y_vals = []
    for lon, lat in zip(longitudes, latitudes):
        x, y = transform(from_proj, to_proj, lon, lat)
        x_vals.append(x)
        y_vals.append(y)
    return x_vals, y_vals

fig = figure(title="Los Angeles Water Usage 2012-2013", plot_width=600,
                plot_height=600, tools=["wheel_zoom", "pan"],
                x_range=(-13209490, -13155375), y_range=(3992960, 4069860),
                webgl=True, active_scroll="wheel_zoom")
fig.axis.visible = False
STAMEN_TONER_BACKGROUND = WMTSTileSource(
    url='http://tile.stamen.com/toner-background/{Z}/{X}/{Y}.png',
    attribution=(
        'Map tiles by <a href="http://stamen.com">Stamen Design</a>, '
        'under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>.'
        'Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, '
        'under <a href="http://www.openstreetmap.org/copyright">ODbL</a>'
    )
)
background = fig.add_tile(STAMEN_TONER_BACKGROUND)

# Do whatever you need to in order to display the figure here

data = requests.get("https://data.lacity.org/resource/v87k-wgde.json").json()

x = []
y = []
use = []

for sample in data:
    x.append(sample['location_1']['coordinates'][0])
    y.append(sample['location_1']['coordinates'][1])
    use.append(int(sample['fy_12_13']))
use_array = np.array(use)
use = list((use_array / float(max(use_array)) * 4 + 3).astype(np.int))
x, y = convert(x,y)

data_dict = {'x':x, 'y':y, 'use':use}
data_source = ColumnDataSource(data=data_dict)
s_circles = fig.circle('x','y', source=data_source, size='use', 
                       fill_color='navy', line_color='navy')

show(fig)
