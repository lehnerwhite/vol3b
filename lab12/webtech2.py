import json
import datetime
import requests
import pandas as pd
import numpy as np
import xml.etree.ElementTree as et

from pyproj import Proj, transform
from scipy.spatial import cKDTree

# Problem 1
class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return {'dtype': 'DateTime', 'data': {'year': str(obj.year), 'month': str(obj.month), 'day': str(obj.day), 'hour': str(obj.hour), 'minute': str(obj.minute), 'second': str(obj.second), 'microsecond': str(obj.microsecond)} }
        return json.JSONEncoder.default(self, obj)
            
def DateTimeDecoder(item):
    accepted_dtypes = {'DateTime': datetime.datetime}
    type = accepted_dtypes.get(item['dtype'], None)
    if type is not None and 'data' in item:
        return type(int(item['data']['year']),int(item['data']['month']), int(item['data']['day']), int(item['data']['hour']), int(item['data']['minute']), int(item['data']['second']), int(item['data']['microsecond']))
    return item

def test1():
    d = datetime.datetime.now()
    enc = DateTimeEncoder()
    encoded = enc.default(d)
    decoded = DateTimeDecoder(encoded)
    print d
    print decoded
    print d == decoded


# Problem 2
# Provide a solution to this problem in a separate file

# DONE


# Problem 3
def prob3():
    # Your code to get the answers here
    f = et.parse('books.xml')
    root = f.getroot()
    books = []
    columns = []
    for book in root:
        info = []
        for attr in book:
            info.append(attr.text)
            if attr.tag not in columns:
                columns.append(attr.tag)
        books.append(info)
    data = pd.DataFrame(books, columns=columns)
    data['price'] = pd.to_numeric(data['price']) 
    data['publish_date'] = pd.to_datetime(data['publish_date'])

    answer1 = data['author'].loc[data['price'].idxmax()]
    answer2 = len(data[data['publish_date'] < datetime.datetime(2000,12,01)])    
    answer3 = 0 
    for idx in xrange(len(data)):
        if data.loc[idx]['description'].find('Microsoft') != -1:
            answer3 += 1
    print "The author with the most expensive book is", answer1         
    print "The number of books published before Dec 1, 2000 is", answer2
    print "The books that reference Microsoft in their description are", str(answer3)



if __name__ == "__main__":
    pass
